@extends('layout.app')

{{-- @section('title', 'Modificar Producto') --}}


@section('script')
<script src="{{asset('js/app/catalogos/productos/registrar.js')}}"></script> 
@endsection


{{-- @section('breadcumb')
    <a class="breadcrumb-item active">Modificar Producto</a>
@endsection --}}

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="card-title">Modificar Producto</h3>
                        </div>
                        <div class="col-md-12">
                            {!! Form::open(["route" => "CatalogoProductosActualizar", "method" => "POST", "id" => "formularioagregar"]) !!}
                            {{ csrf_field() }}
                            {!! Form::hidden('id',$producto->id,["id"=>"id"]) !!}
                                <div class="row">

                                    <div class="col-sm-6">
                                        <span class="text-danger">*</span>{!! Form::label('nombre','Nombre producto') !!}
                                        {!! Form::text('nombre',$producto->nombreProducto, ['id' => 'nombre', 'class' => 'form-control', 'maxlength','placeholder'=> 'Nombre producto' ]) !!}
                                    </div>
                                    {{-- <div class="col-sm-6">
                                        <span class="text-danger">*</span>{!! Form::label('provedor','Provedor') !!}
                                        {!! Form::text('provedor',null, ['id' => 'provedor', 'class' => 'form-control', 'maxlength','placeholder'=> 'Provedo' ]) !!}
                                    </div> --}}

                                    <div class="col-md-6">
                                        <span class="text-danger">*</span>{!! Form::label('provedor','Provedor') !!}
                                        <div class="d-flex justify-content-between">
                                            <select class="custom-select" name="provedor" id="provedor">
                                                @foreach ($provedores as $provedor)
                                                    <option value="{{ $provedor->nombre}}">{{ $provedor->nombre}}</option>
                                                @endforeach
                                            </select> 
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <span class="text-danger">*</span>{!! Form::label('piezas','Piezas') !!}
                                        {!! Form::number('piezas',$producto->piezas, ['id' => 'piezas', 'class' => 'form-control', 'maxlength','placeholder'=> 'Piezas' ]) !!}
                                    </div>

                                    <div class="col-sm-6">
                                        <span class="text-danger">*</span>{!! Form::label('precioCosto','Precio costo') !!}
                                        {!! Form::text('precioCosto',$producto->precioCosto, ['id' => 'precioCosto', 'class' => 'form-control', 'maxlength','placeholder'=> 'Precio costo' ]) !!}
                                    </div>

                                    <div class="col-sm-6">
                                        <span class="text-danger">*</span>{!! Form::label('precioVenta','Precio venta') !!}
                                        {!! Form::text('precioVenta',$producto->precioVenta, ['id' => 'precioVenta', 'class' => 'form-control', 'maxlength' => '10','placeholder'=> 'Precio venta' ]) !!}
                                    </div>

                            {!! Form::close() !!}
                        </div>
                        <div class="col-sm-2 -sm-8 " style="margin-top: 4%; float:left">
                            <a class="btn btn-sm btn-light btn-block btn-rounded" href="{{route('CatalogoProductosIndex')}}"><i class="fa fa-arrow-left">
                            </i><span class="d-none d-lg-block" > Volver</span></a>
                        </div>
                        <div class="col-sm-2 offset-sm-8" style="margin-top: 4%;float:right">
                            <button  type="submite" onclick="update(this);"class="btn btn-dark btn-block btn-rounded"><i class="fa fa-plus-square" ></i>
                            <span class="d-none d-lg-block"> Actualizar</span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection