{{-- 
<div class="col-md-12">
    <div class="row">
        <div class="col-md-10">
            <h2 class="card-title" style="color: black">Productos</h2>
        </div>
      
    </div>

    <div class="table-responsive" >
        <table  class="table table-borderless" >
          
            <thead style="color: black">
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Nombre Producto</th>
                  <th scope="col">Piezas Restantes</th>
                </tr>
            </thead>
            
            <tbody >
                @foreach ($piezas as $key => $piez)
                    <tr>
                        <td width="5%">{{$key+1}}</td>
                        <td >{{$piez->nombreProducto}}</td>
                        <td >{{$piez->piezas}}</td>
                       
                    <tr>
                @endforeach

            </tbody>

        </table>
    </div>

</div> --}}


<div class="modal"  style="justify-content: center;">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Productos</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <div class="table-responsive" >
                <table  class="table table-borderless" >
                  
                    <thead style="color: black">
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Nombre Producto</th>
                          <th scope="col">Piezas Restantes</th>
                        </tr>
                    </thead>
                    
                    <tbody >
                        @foreach ($piezas as $key => $piez)
                            <tr>
                                <td width="5%">{{$key+1}}</td>
                                <td >{{$piez->nombreProducto}}</td>
                                <td >{{$piez->piezas}}</td>
                                {{-- <td >{{$producto->provedor}}</td>
                                <td >{{$producto->precioVenta}}</td> --}}
                                
                            <tr>
                        @endforeach
        
                    </tbody>
        
                </table>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
          {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
        </div>
      </div>
    </div>
</div>


