@extends('template.app')

@section('title','Provedores')

@section('script')
@endsection

@section('content')
<div class="col-md-12">
    <div class="row">
        <div class="col-md-10">
            <h2 class="card-title" style="color: black">Provedores</h2>
        </div>
        <div class="col-md-2" >
            <a class="btn btn-dark btn-block btn-rounded" href="{{route("CatalogoProvedoresRegistrar")}}" >
            <i class="fa fa-plus-square" ></i><span class="d-none d-lg-block" style="width: auto;" > Registrar</span></a>
        </div>
    </div>

    <div class="table-responsive" >
        <table  class="table table-borderless" >
          
            <thead style="color: black">
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Provedor</th>
                  <th scope="col">Dia</th>
                  <th scope="col">Acciones</th>
                </tr>
            </thead>
            
            <tbody >
                @foreach ($provedores as $key => $provedor)
                    <tr>
                        <td width="5%">{{$key+1}}</td>
                        <td >{{$provedor->nombre}}</td>
                        <td >{{$provedor->diaVisita}}</td> 
                        {{-- <td >{{$producto->provedor}}</td>
                        {{-- <td >{{$producto->precioVenta}}</td> --}}
                        <td align="center" width="5%">
                            <span style="font-size: 25px;">
                                <a href="{{ route("CatalogoProvedoresModificar",$provedor->id)}}">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
                                        <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                        <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                      </svg>
                                </a>
                            </span>
                            <span style="font-size: 25px;">
                                <a class="text-danger eliminar" href="{{ route("CatalogoProvedoresEliminar",$provedor->id)}}">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash" viewBox="0 0 16 16">
                                        <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5Zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6Z"/>
                                        <path d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1ZM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118ZM2.5 3h11V2h-11v1Z"/>
                                      </svg>
                                </a>
                            </span>
                        </td> 
                   
                    <tr>
                @endforeach

            </tbody>

        </table>
    </div>

</div>
@endsection

@section('js')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script>
   $('.eliminar').on('click',function(event) {
        event.preventDefault();
        const url = $(this).attr('href');

        Swal.fire({
title: 'Estas seguro de eliminar?',
text: "Sera eliminado permanentemente, no podrás revertir esto!",
icon: 'warning',
showCancelButton: true,
confirmButtonColor: '#3085d6',
cancelButtonColor: '#d33',
cancelButtonText:'Cancelar',
confirmButtonText: 'Si, eliminarlo!'
}).then((result) => {
if (result.isConfirmed) {
window.location.href = url

Swal.fire(
  'Eliminado!',
  'Tu archivo ha sido eliminado.',
  'success'
)
}
})
    })


</script>


@endsection
