@extends('template.app')

@section('title', 'Registrar Provedor')


@section('script')
<script src="{{asset('js/app/catalogos/provedores/registrar.js')}}"></script> 
@endsection


@section('breadcumb')
    <a class="breadcrumb-item active">Registrar Provedor</a>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h3 class="card-title">Registrar Provedor</h3>
                        </div>
                        <div class="col-md-12">
                            {!! Form::open(["route" => "CatalogoProvedoresAgregar", "method" => "POST", "id" => "formularioagregar"]) !!}
                                {{ csrf_field() }}
                                <div class="row">

                                    <div class="col-sm-6">
                                        <span class="text-danger">*</span>{!! Form::label('nombre','Nombre provedor') !!}
                                        {!! Form::text('nombre',null, ['id' => 'nombre', 'class' => 'form-control', 'maxlength','placeholder'=> 'Nombre provedor' ]) !!}
                                    </div>
                                  
                                    <div class="col-sm-6">
                                        <span class="text-danger">*</span>{!! Form::label('dia','Dia de visita') !!}
                                        {!! Form::text('dia',null, ['id' => 'dia', 'class' => 'form-control', 'maxlength','placeholder'=> 'Dia de visita' ]) !!}
                                    </div>

                            {!! Form::close() !!}
                        </div>
                        <div class="col-sm-2 -sm-8 " style="margin-top: 4%; float:left">
                            <a class="btn btn-sm btn-light btn-block btn-rounded" href="{{route("CatalogoProvedoresIndex")}}"><i class="fa fa-arrow-left">
                            </i><span class="d-none d-lg-block" > Volver</span></a>
                        </div>
                        <div class="col-sm-2 offset-sm-8" style="margin-top: 4%;float:right">
                            <button  type="submite" onclick="registrar(this);"class="btn btn-dark btn-block btn-rounded"><i class="fa fa-plus-square" ></i>
                            <span class="d-none d-lg-block"> Agregar</span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection