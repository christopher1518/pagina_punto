<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Register</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" 
        integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
        <link rel="stylesheet" href="{{asset('css/app.css')}}">
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    </head>

    <body>
        <div class="container d-flex">
            <form action="" method="POST" class="m-auto bg-white p-5 rounded-sm shadow-lg w-form">
                @csrf
                <h2>
                    Registrarse
                </h2>
                {{-- email --}}
                <div class="form-group">
                    <label for="exampleImputEmail1">Email</label>
                    <input name="email" type="email" value="{{ old ('email') }}" class="form-control" id="exampleImputEmail1" 
                     aria-describedby="emailHelp" placeholder="Enter email">
                            @error('email')
                                <small class="text-danger nt-1">
                                        <strong>{{ $message}}</strong>
                                </small>  
                            @enderror
                </div>
                {{--contraseña --}}
                <div class="form-group">
                    <label for="exampleImputPassword1">Password</label>
                    <input  type="password" name="password" class="form-control" id="exampleImputPassword1" 
                        placeholder="Password"> 
                            @error('password')
                                <small class="text-danger nt-1">
                                        <strong>{{ $message}}</strong>
                                </small>  
                            @enderror 
                </div>
                {{--condirmacionContraseña --}}
                <div class="form-group">
                    <label for="exampleImputPassword1">Password confirmation</label>
                    <input   name="password_confirmation" type="password" class="form-control" id="exampleImputPassword1" 
                        placeholder="Password"> 
                            @error('password_confirmation')
                                <small class="text-danger nt-1">
                                        <strong>{{ $message}}</strong>
                                </small>  
                            @enderror  
                </div>
                <br>
                {{-- linckRegistrarse --}}
                <div class="d-grid gap-2 col-6 mx-auto">
                    <button type="submit" class="btn btn-primary btn-block ">Registrarse</button>
                </div>
                  {{-- iniciarSesion --}}
                <div class="nt-3 text-center">
                    <a href=" {{ route('login')}}">Iniciar sesion</a>

                </div>
            </form>
        </div>

        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js" 
        integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.min.js" 
        integrity="sha384-Rx+T1VzGupg4BHQYs2gCW9It+akI2MM/mndMCy36UVfodzcJcF0GGLxZIzObiEfa" crossorigin="anonymous"></script>
        @include('sweet::alert')
    </body>
</html>