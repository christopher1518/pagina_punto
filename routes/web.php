<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\AuthController;
// use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('auth.login');
});

// grupo auth/
Route::prefix('auth')->group(function(){
    // login
    Route::get('login',[AuthController::class,'login'])->name('login');
    Route::post('login',[AuthController::class,'loginVerify'])->name('login.verify');
    // register
    Route::get('register',[AuthController::class,'register'])->name('register');
    Route::post('register',[AuthController::class,'registerVerify']);
    Route::post('singOut',[AuthController::class,'singOut'])->name('singOut');

});

// protegidas
Route::middleware('auth')->group(function(){
    Route::get('loginVerify',function(){
        return view('app.inicio.index');
    })->name('inicio');
});


Route::group(["prefix" => "/app", 'middleware' => ['auth']], function () {
  
    Route::group(["prefix"=> "/productos", 'middleware' => ['auth']], function(){

       Route::get('/', [
           'uses' => 'App\Http\Controllers\Catalogos\ProductosController@index',
           'as' => 'CatalogoProductosIndex'
       ]);
       
       Route::get('/res', [
           'uses' => 'App\Http\Controllers\Catalogos\ProductosController@restantes',
           'as' => 'CatalogoProductosPiezas'
       ]);



       Route::get('/registrar', [
           'uses' => 'App\Http\Controllers\Catalogos\ProductosController@registrar',
           'as' => 'CatalogoProductosRegistrar'
       ]);

       Route::post('/agregar', [
           'uses' => 'App\Http\Controllers\Catalogos\ProductosController@agregar',
           'as' => 'CatalogoProductosAgregar'
       ]);

       Route::get('/modificar/{id}', [
           'uses' => 'App\Http\Controllers\Catalogos\ProductosController@modificar',
           'as' => 'CatalogoProductosModificar'
       ]);
      
       Route::post('/actualizar', [
           'uses' => 'App\Http\Controllers\Catalogos\ProductosController@actualizar',
           'as' => 'CatalogoProductosActualizar'
       ]);
      
       Route::get('/eliminar/{id}', [
           'uses' => 'App\Http\Controllers\Catalogos\ProductosController@eliminar',
           'as' => 'CatalogoProductosEliminar'
       ]);
    }); //  finGrupoProductos

    Route::group(["prefix"=> "/provedores", 'middleware' => ['auth']], function(){

       Route::get('/', [
           'uses' => 'App\Http\Controllers\Catalogos\ProvedoresController@index',
           'as' => 'CatalogoProvedoresIndex'
       ]);

       Route::get('/registrar', [
           'uses' => 'App\Http\Controllers\Catalogos\ProvedoresController@registrar',
           'as' => 'CatalogoProvedoresRegistrar'
       ]);

       Route::post('/agregar', [
           'uses' => 'App\Http\Controllers\Catalogos\ProvedoresController@agregar',
           'as' => 'CatalogoProvedoresAgregar'
       ]);

       Route::get('/modificar/{id}', [
           'uses' => 'App\Http\Controllers\Catalogos\ProvedoresController@modificar',
           'as' => 'CatalogoProvedoresModificar'
       ]);
      
       Route::post('/actualizar', [
           'uses' => 'App\Http\Controllers\Catalogos\ProvedoresController@actualizar',
           'as' => 'CatalogoProvedoresActualizar'
       ]);
      
       Route::get('/eliminar/{id}', [
           'uses' => 'App\Http\Controllers\Catalogos\ProvedoresController@eliminar',
           'as' => 'CatalogoProvedoresEliminar'
       ]);
    }); //  finGrupoProvedores

    Route::group(["prefix"=> "/gastos", 'middleware' => ['auth']], function(){

       Route::get('/', [
           'uses' => 'App\Http\Controllers\Catalogos\GastosController@index',
           'as' => 'CatalogoGastosIndex'
       ]);

       // Route::get('/registrar', [
       //     'uses' => 'App\Http\Controllers\Catalogos\ProvedoresController@registrar',
       //     'as' => 'CatalogoProvedoresRegistrar'
       // ]);

       // Route::post('/agregar', [
       //     'uses' => 'App\Http\Controllers\Catalogos\ProvedoresController@agregar',
       //     'as' => 'CatalogoProvedoresAgregar'
       // ]);

       // Route::get('/modificar/{id}', [
       //     'uses' => 'App\Http\Controllers\Catalogos\ProvedoresController@modificar',
       //     'as' => 'CatalogoProvedoresModificar'
       // ]);
      
       // Route::post('/actualizar', [
       //     'uses' => 'App\Http\Controllers\Catalogos\ProvedoresController@actualizar',
       //     'as' => 'CatalogoProvedoresActualizar'
       // ]);
      
       // Route::get('/eliminar/{id}', [
       //     'uses' => 'App\Http\Controllers\Catalogos\ProvedoresController@eliminar',
       //     'as' => 'CatalogoProvedoresEliminar'
       // ]);
    }); //  finGrupoGastos
    
});
// Route::get('/', function () {
//     return view('catalogos.index');
// });
