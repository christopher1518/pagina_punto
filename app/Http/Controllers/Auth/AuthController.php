<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    public function register(){
        return view('auth.register');
    }

    public function registerVerify(Request $request){
        $request->validate([
            'email' => 'required|unique:users,email|email',
            'password' => 'required|min:4',
            'password_confirmation' => 'required|same:password',
        ],[
            'email.required' => 'el email es requerido en español' ,
            'email.unique' => 'el email ya ha sido usado' ,
            'password.required' => 'la contraseña es requerida' ,
            'password_confirmation.required' => 'la confirmacion de contraseña es requerida' ,
        ]);

        User::create([
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);
        alert()->success('El usuario ya esta registradoe', 'Exito');
        return redirect()->route('register');
        // return  redirect()->route('login')->with('succes','El usuario ya esta registrado');
    }

    public function login(){
        return view('auth.login');
    }

    public function loginVerify(Request $request){
       $request->validate([
            'email' =>'required|email',
            'password'=> 'required|min:4',
       ],[
            'email.required' => 'el email es requerido en español' ,
            'password.required' => 'la contraseña es requerida' ,
       ]);
       if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
        return redirect()->route('inicio');
       }
    //    return back()->warning(['invalid_credentials'=>'Usuario o contraseña invalido'])->autoclose(5000);
    alert()->warning('Usuario o contraseña invalido', 'Error');
    return back();
    //    return back()->withErrors(['invalid_credentials'=>'Usuario o contraseña invalido'])->withInput();

    }

    public function singOut(){
        Auth::logout();
        alert()->success('session cerrada correctamente', 'Exito');
        return redirect()->route('login');
        // return redirect()->route('login')->with('success','session cerrada correctamente');
    }
}
