<?php

namespace App\Http\Controllers\Catalogos;

use App\Http\Controllers\Controller;
use App\Models\Provedor;
use Illuminate\Http\Request;


class ProvedoresController extends Controller
{
    public function index(){

       $provedores = Provedor::where('estado','1')->orderby('nombre', 'asc')->get();
       return view('app.catalogos.provedores.index',
       ['provedores' => $provedores]);

        // = Provedor::where('estado', '1')->orderby('nombre', 'asc')->get();
    //     return view('app.catalogos.provedores.index',[
    //     "provedores" => $provedores]
    // );
    }

    public function registrar(){

        // $provedores = Provedor::all();
        return view('app.catalogos.provedores.registrar'
        // ,["provedores" => $provedores]
        );
    }

    public function agregar(Request $request)
    {
        $data = $request->all();
  
        $producto = new Provedor();
        $producto-> nombre = $data["nombre"];
        $producto-> diaVisita = $data["dia"];
        $producto->save();
  
        return redirect()->route('CatalogoProvedoresIndex');
     }

     public function modificar($id)
   {
        $provedores = Provedor::find($id);
         return view('app.catalogos.provedores.modificar'
        ,["provedores" => $provedores]
         );
   }


   public function actualizar(Request $request){
     $data = $request->all();

      $provedor = Provedor::find($data["id"]);
      $provedor-> nombre = $data["nombre"];
      $provedor-> diaVisita = $data["dia"];
      $provedor->save();
      alert()->success('Provedor actualizado correctamente.')->persistent("Close");
      return redirect()->route('CatalogoProvedoresIndex');

   }

   public function eliminar($id){
      $provedor = Provedor::find($id);
        $provedor -> estado = "2";
        $provedor  -> save();
      //   alert()->success('Producto eliminado correctamente.');
        return redirect()->route('CatalogoProvedoresIndex');
   }
}
