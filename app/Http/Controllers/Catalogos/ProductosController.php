<?php

namespace App\Http\Controllers\Catalogos;

use App\Http\Controllers\Controller;
use App\Models\Producto;
use App\Models\Provedor;
use Illuminate\Http\Request;

class ProductosController extends Controller
{
   public function index()
   {
      $productos = Producto::where('estado', '1')->orderby('nombreProducto', 'asc')->get();
   
      return view('app.catalogos.productos.index',[
         "productos" => $productos]);
   }

   public function restantes()
   {
      $piezas = Producto::where([['piezas','3'],['estado','1']])->first();
      if($piezas !=null) {
        alert()->error('Producto menor a 3.')->persistent("Close");
        return view('app.catalogos.productos.ajax.piezas',
      [
         'piezas' => $piezas
      ]);
      }
   }



   public function registrar()
   {
      $provedores = Provedor::where('estado', '1')->orderby('nombre', 'asc')->get();

      return view('app.catalogos.productos.registrar'
      ,["provedores" => $provedores]
      );
   }

   public function agregar(Request $request){
      $data = $request->all();

      $producto = new Producto();
      $producto-> nombreProducto = $data["nombre"];
      $producto-> provedor = $data["provedor"];
      $producto-> piezas = $data['piezas'];
      $producto-> precioVenta = $data["precioVenta"];
      $producto-> precioCosto = $data["precioCosto"];
      $producto->save();

      return redirect()->route('CatalogoProductosIndex');
   }

   public function modificar($id)
   {
      $producto = Producto::find($id);
      $provedores = Provedor::where('estado', '1')->orderby('nombre', 'asc')->get();
      return view('app.catalogos.productos.modificar'
      ,["provedores" => $provedores,
       "producto" => $producto]
      );
   }


   public function actualizar(Request $request){
      $data = $request->all();

      $producto = Producto::find($data['id']);
      $producto-> nombreProducto = $data["nombre"];
      $producto-> provedor = $data["provedor"];
      $producto-> piezas = $data['piezas'];
      $producto-> precioVenta = $data["precioVenta"];
      $producto-> precioCosto = $data["precioCosto"];
      $producto->save();
        alert()->success('Producto actualizado correctamente.')->persistent("Close");
      return redirect()->route('CatalogoProductosIndex');
   }

   public function eliminar($id){
      $producto = Producto::find($id);
        $producto -> estado = "2";
        $producto -> save();
      //   alert()->success('Producto eliminado correctamente.');
        return redirect()->route('CatalogoProductosIndex');
   }

}
